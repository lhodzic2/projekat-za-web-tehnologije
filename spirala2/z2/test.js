let assert = chai.assert;

describe('iscrtajModul.js',function() {
    describe("iscrtajRaspored()", function() {
        //1
        it('testiramo da li raspored iscrtava ispravan broj redova',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21)
            let tabela = okvir.firstChild;
            assert.equal(tabela.childElementCount,6,'Broj redova treba biti sest');
            okvir.innerHTML = "";
        });
        //2
        it('da li dolazi do greske ako je satPocetak veci od satKraj',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,5);
            let poruka = okvir.innerHTML;
            assert.equal(poruka, 'Greška','U div treba upisati tekst \'Greška\'');
            okvir.innerHTML = "";
        });
        //3
        it('da li dolazi do greske ako je niz dana prazan',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,[],5,8);
            let poruka = okvir.innerHTML;
            assert.equal(poruka, 'Greška','U div treba upisati tekst \'Greška\'');
            okvir.innerHTML = "";
        });
        //4
        it('da li radi kada broj dana nije pet',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak","subota"],5,8);
            let tabela = document.getElementsByTagName('table');
            assert.equal(tabela[0].childElementCount,7,'Broj redova treba biti sedam');
            okvir.innerHTML = "";
        });
        //5
        it('da li ispravno ispisuje dane',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak","Subota"],5,8);
            let tabela = document.getElementsByTagName('table');
            assert.equal(tabela[0].childNodes[3].firstChild.innerHTML,'Srijeda','Dan u tecem redu treba biti srijeda');
            okvir.innerHTML = "";
        });
        //6
        it('da li ispisuje sate koje ne bi trebalo',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,15);
            let tabela = okvir.firstChild;
            let prviRed = tabela.firstChild;
            assert.equal(prviRed.childNodes[10].innerHTML,'','Treba biti prazna celija');
            okvir.innerHTML = "";
        });
        //7
        it('da li ispisuje posljednji sat',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,15);
            let tabela = okvir.firstChild;
            let prviRed = tabela.firstChild;
            assert.equal(prviRed.childElementCount,14,'');
            okvir.innerHTML = "";
        });
        //8
        it('ispisivanje greske kada satKraj nije cijeli broj',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,12.5);
            let poruka = okvir.innerHTML;
            assert.equal(poruka, 'Greška','U div treba upisati tekst \'Greška\'');
            okvir.innerHTML = "";
        });
        //9
        it('ispisivanje greske kada satPocetak nije cijeli broj',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8.6,15);
            let poruka = okvir.innerHTML;
            assert.equal(poruka, 'Greška','U div treba upisati tekst \'Greška\'');
            okvir.innerHTML = "";
        });
        //10
        it('ispisivanje greske kada su satPocetk i satKraj jednaki',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,8);
            let poruka = okvir.innerHTML;
            assert.equal(poruka, 'Greška','U div treba upisati tekst \'Greška\'');
            okvir.innerHTML = "";
        });
        //11
        it('dodavanje aktivnosti u neispravno vrijeme',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",14,12,"Ponedjeljak");
            assert.equal(poruka,"pogresno vrijeme");
            okvir.innerHTML = "";
        });
        //12
        it('dodavanje aktivnosti kada raspored nije kreiran',function() {
            let okvir = document.getElementById('okvir');
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",14,12,"Ponedjeljak");
            assert.equal(poruka,"raspored nije kreiran");
            okvir.innerHTML = "";
        });
        //13
        it('dodavanje aktivnosti kada vrijeme pocetka nije u ispravnom formatu',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12.1,14,"Ponedjeljak");
            assert.equal(poruka,"ne postoji vrijeme");
            okvir.innerHTML = "";
        });
        //14
        it('dodavanje aktivnosti kada vrijeme kraja nije u ispravnom formatu',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14.2,"Ponedjeljak");
            assert.equal(poruka,"ne postoji vrijeme");
            okvir.innerHTML = "";
        });
        //15
        it('dodavanje aktivnosti kada vrijeme kraja nije u ispravnom formatu',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14.2,"Ponedjeljak");
            assert.equal(poruka,"ne postoji vrijeme");
            okvir.innerHTML = "";
        });
        //16
        it('dodavanje aktivnosti kada vrijeme pocetka i vrijeme kraja nisu u ispravnom formatu',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12.1,14.2,"Ponedjeljak");
            assert.equal(poruka,"ne postoji vrijeme");
            okvir.innerHTML = "";
        });
        //17
        it('dodavanje aktivnosti kada dan ne postoji',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14,"Subota");
            assert.equal(poruka,"pogresno vrijeme");
            okvir.innerHTML = "";
        });
        //18
        it('dodavanje aktivnosti kada se termini potpuno poklapaju',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14,"Ponedjeljak");
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14,"Ponedjeljak");
            assert.equal(poruka,"zauzet termin");
            okvir.innerHTML = "";
        });

        //19
        it('dodavanje aktivnosti kada se termini djelimicno poklapaju',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14,"Ponedjeljak");
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,13,"Ponedjeljak");
            assert.equal(poruka,"zauzet termin");
            okvir.innerHTML = "";
        });
        //20
        it('dodavanje aktivnosti u termin koji nije u rasporedu',function() {
            let okvir = document.getElementById('okvir');
            iscrtajModul.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,20);
            iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",12,14,"Ponedjeljak");
            var poruka = iscrtajModul.dodajAktivnost(okvir,"WT","predavanje",7,9,"Ponedjeljak");
            assert.equal(poruka,"pogresno vrijeme");
            okvir.innerHTML = "";
        });
  
    });
});