const express = require('express');
const db = require('./db');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');

app.listen(3000);
app.use(bodyParser.json());

db.sequelize.sync({force:true}).then(() => console.log('konektovana'));
app.use(express.static('public'))

app.get('/studenti.html',function(req,res) {
    res.sendFile(path.join(__dirname + "/public/studenti.html"));
});

//CREATE metode

app.post('/v2/student', function (req,res) {
    let tijelo = req.body;
    db.student.findOne({where: {index: tijelo.index}}).then((student) => {
        if (student == null) {
            db.student.create(tijelo).then(() => {
                res.json({message:"Uspješno dodan student!"});
            },() => {
                res.json({message:"Greška"});
            });
        } else {
            res.json({message:"Greška - student sa istim indexom već postoji"});
        }
    });
});

app.post('/v2/tip', function (req,res) {
    let tijelo = req.body;
    db.tip.findOne({where: {naziv: tijelo.naziv}}).then((tip) => {
        if (tip == null) {
            db.tip.create(tijelo).then(() => {
                res.json({message:"Uspješno dodan tip!"});
            },() => {
                res.json({message:"Greška"});
            });
        } else {
            res.json({message:"Greška - tip već postoji!"});
        }
    });
    
});

app.post('/v2/dan', function (req,res) {
    let tijelo = req.body;
    db.dan.findOne({where: {naziv:tijelo.naziv}}).then((dan) => {
        if (dan == null) {
            db.dan.create(tijelo).then(() => {
                res.json({message:"Uspješno dodan dan!"});
            },() => {
                res.json({message:"Greška"});
            });
        } else {
            res.json({message:"Greška - dan već postoji!"});
        }
    });
    
});

app.post('/v2/grupa', function (req,res) {
    let tijelo = req.body;
    db.grupa.create({naziv: tijelo.naziv, predmetId: tijelo.predmet}).then(() => {
        res.json({message:"Uspješno dodana grupa!"});
    },() => {
        res.json({message:"Greška"});
    });
});

app.post('/v2/predmet', function (req,res) {
    let tijelo = req.body;
    db.predmet.findOne({where: {naziv: tijelo.naziv}}).then((predmet) => {
        if (predmet == null) {
            db.predmet.create(tijelo).then(() => {
                res.json({message:"Uspješno dodan predmet!"});
                },() => {
                    res.json({message:"Greška"});
                });
            } else {
                res.json({message:"Greška-predmet već postoji"});
            }
    });
    
});

app.post('/v2/aktivnost', function (req,res) {
    let tijelo = req.body;
    db.aktivnost.create({naziv:tijelo.naziv, pocetak:tijelo.pocetak, kraj: tijelo.kraj,predmetId:tijelo.predmet,grupaId:tijelo.grupa,danId:tijelo.dan, tipId:tijelo.tip}).then(() => {
        res.json({message:"Uspješno dodana aktivnost!"});
    },() => {
        res.json({message:"Greška"});
    });
});

//READ metode

app.get('/v2/student',function (req,res) {
    db.student.findAll().then(student => res.json(student));
});

app.get('/v2/aktivnost',function (req,res) {
    db.aktivnost.findAll().then(aktivnost => res.json(aktivnost));
});

app.get('/v2/predmet',function (req,res) {
    db.predmet.findAll().then(predmet => res.json(predmet));
});

app.get('/v2/dan',function (req,res) {
    db.dan.findAll().then(dan => res.json(dan));
});

app.get('/v2/tip',function (req,res) {
    db.tip.findAll().then(tip => res.json(tip));
});

app.get('/v2/grupa',function (req,res) {
    db.grupa.findAll().then(grupa => res.json(grupa));
});

//UPDATE metode

app.put('/v2/student/:id', function(req,res) {
    db.student.update(
    req.body,{ where: {id: req.params.id}}
    ).then(br => {
        res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

app.put('/v2/predmet/:id', function(req,res) {
    db.predmet.update(
        req.body,{ where: {id: req.params.id}}
        ).then(br => {
            res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

app.put('/v2/grupa/:id', function(req,res) {
    db.grupa.update(
        req.body,{ where: {id: req.params.id}}
        ).then(br => {
            res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

app.put('/v2/dan/:id', function(req,res) {
    db.dan.update(
        req.body,{ where: {id: req.params.id}}
        ).then(br => {
            res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

app.put('/v2/tip/:id', function(req,res) {
    db.tip.update(
        req.body,{ where: {id: req.params.id}}
        ).then(br => {
            res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

app.put('/v2/aktivnost/:id', function(req,res) {
    db.aktivnost.update(
        req.body,{where: {id: req.params.id}}
    ).then(br => {
        res.send({message: 'Uspješno izmijenjeno ' + br+ ' redova!'});
    });
});

//DELETE metode

app.delete('/v2/student/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.student.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisan student!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.delete('/v2/predmet/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.predmet.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisan predmet!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.delete('/v2/aktivnost/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.aktivnost.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisana aktivnost!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.delete('/v2/grupa/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.grupa.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisana grupa!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.delete('/v2/tip/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.tip.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisan tip!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.delete('/v2/dan/:id', function(req,res) {
    let id = Number.parseInt(req.params.id);
    db.dan.destroy( {
        where: {
            id: id
        }
    }).then(() => {
        res.json({message: 'Uspješno obrisan dan!'});
    },() => {
        res.json({message: 'Greška'});
    })
});

app.post('/v2/student_grupa',function(req,res) {
    let niz = req.body;

    for (let i = 0; i < niz.length; i++){
        let student= niz[i];
        db.grupa.findOne({where:{naziv:student.grupa}}).then((gr) => {
            db.student.findOrCreate({where:{index:student.index,ime:student.ime}}).then((st => {
                gr.addStudent(st);
            }))
        });
    }
    let string = "Uspješno dodano " + niz.length + " redova!"
    res.send({message:string});
});

