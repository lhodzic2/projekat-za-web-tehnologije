const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018389","root","root",{host:"localhost",dialect:"mysql",logging:false});
module.exports = sequelize;
const db={};
const path = require('path');

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.predmet = require(path.join(__dirname, '/modeli/predmet.js'))(sequelize, Sequelize.DataTypes);
db.grupa = require(path.join(__dirname, '/modeli/grupa.js'))(sequelize, Sequelize.DataTypes);
db.aktivnost = require(path.join(__dirname, '/modeli/aktivnost.js'))(sequelize, Sequelize.DataTypes);
db.dan = require(path.join(__dirname, '/modeli/dan.js'))(sequelize, Sequelize.DataTypes);
db.tip = require(path.join(__dirname, '/modeli/tip.js'))(sequelize, Sequelize.DataTypes);
db.student = require(path.join(__dirname, '/modeli/student.js'))(sequelize, Sequelize.DataTypes);

db.predmet.hasMany(db.grupa,{foreignKey: {allowNull: false}});
db.grupa.belongsTo(db.predmet,{foreignKey: {allowNull: false}});

db.predmet.hasMany(db.aktivnost,{foreignKey: {allowNull: false}});
db.aktivnost.belongsTo(db.predmet,{foreignKey: {allowNull: false}});

db.grupa.hasMany(db.aktivnost,{foreignKey: {allowNull: true}});
db.aktivnost.belongsTo(db.grupa,{foreignKey: {allowNull: true}});

db.dan.hasMany(db.aktivnost,{foreignKey: {allowNull: false}});
db.aktivnost.belongsTo(db.dan,{foreignKey: {allowNull: false}});

db.tip.hasMany(db.aktivnost,{foreignKey: {allowNull: false}});
db.aktivnost.belongsTo(db.tip,{foreignKey: {allowNull: false}});

db.studentGrupa=db.student.belongsToMany(db.grupa,{as:'grupa',through:'student_grupa',foreignKey:'student'});
db.grupa.belongsToMany(db.student,{as:'student',through:'student_grupa',foreignKey:'grupa'});


module.exports=db;