var ajax = new XMLHttpRequest();
var getGrupa = new XMLHttpRequest();
var getStudent = new XMLHttpRequest();
var studentGrupa = new XMLHttpRequest();
var dugme = document.getElementById('dugme');
var studenti = [];
var grupe = [];

getStudent.open('GET','http://localhost:3000/v2/student',true);
getStudent.onload = function() {
    studenti = JSON.parse(getStudent.responseText); 
    console.log(studenti);
}
getStudent.send();

getGrupa.open('GET','http://localhost:3000/v2/grupa',true);
getGrupa.onload = function() {
    grupe = JSON.parse(getGrupa.responseText);
    console.log(grupe);
    let select = document.getElementById('grupa');
    for (let i = 0; i < grupe.length; i++) {
        let opcija = document.createElement('option');
        opcija.setAttribute('value',grupe[i].naziv);
        opcija.appendChild(document.createTextNode(grupe[i].naziv));
        select.appendChild(opcija);
    }
}
getGrupa.send();

dugme.onclick = function(){
    let unos = document.getElementById('text').value;
    let grupa = document.getElementById('grupa').value;
    if (unos.indexOf(',') == -1) {
        alert('Unos nije u ispravnom formatu');
        return;
    }
    let redovi = unos.split('\n');
    let niz = [];
    var bioIsti = false;
    var string='';
    for (let i = 0; i < redovi.length; i++) {
        let red = redovi[i].split(',');
        let ime = red[0];
        console.log(ime);
        let index = red[1].replace('\n','').replace('\r','');

        let objekat = {
          ime: ime,
          index: index,
          grupa: grupa
        }
        let isti = false;

        for (let j = 0; j < studenti.length; j++) {
            if (studenti[j].index == index) {
                string = string + 'Student ' + ime + ' nije kreiran jer postoji student ' + studenti[j].ime +' sa istim indexom ' + index + '\n';
                isti = true;
                bioIsti = true;
            }
        }
        if(!isti) niz.push(objekat);
    }
    console.log(niz);
    
    if (niz.length != 0) {
            ajax.open('POST','http://localhost:3000/v2/student_grupa',true);  
            ajax.onload = function() {
                document.getElementById('text').value = document.getElementById('text').value + '\n' + ajax.responseText;
            }
            ajax.setRequestHeader('Content-Type','application/json');
            ajax.send(JSON.stringify(niz));
    }
    if (bioIsti) document.getElementById('text').value = document.getElementById('text').value + '\n' + string;
    return false;
}