DELETE,/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}
GET,/predmeti,null,[]
GET,/aktivnosti,null,[]
POST,/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Uspješno dodan predmet!\"}
POST,/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Naziv predmeta postoji!\"}
POST,/aktivnost,{\"naziv\":\"WT\"},{\"message\":\"Aktivnost nije validna!\"}
POST,/aktivnost,{\"naziv\":\"WT\",\"tip\":\"Vježbe\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"},{\"message\":\"Uspješno dodana aktivnost!\"}
POST,/aktivnost,{\"naziv\":\"DM\",\"tip\":\"Predavanje\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"},{\"message\":\"Uspješno dodana aktivnost!\"}
GET,/predmeti,null,[{\"naziv\":\"RMA\"}]
GET,/aktivnosti,null,[{\"naziv\":\"WT\",\"tip\":\"Vježbe\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"},{\"naziv\":\"DM\",\"tip\":\"Predavanje\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"}]
GET,/predmet/WT/aktivnost,null,[{\"tip\":\"Vježbe\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"}]
POST,/aktivnost,{\"naziv\":\"DM\",\"tip\":\"Predavanje\",\"pocetak\":7,\"kraj\":9,\"dan\":\"Utorak\"},{\"message\":\"Uspješno dodana aktivnost!\"}
GET,/predmet/DM/aktivnost,null,[{\"tip\":\"Predavanje\",\"pocetak\":9,\"kraj\":10,\"dan\":\"Srijeda\"},{\"tip\":\"Predavanje\",\"pocetak\":7,\"kraj\":9,\"dan\":\"Utorak\"}]
DELETE,/aktivnost/WT,null,{\"message\":\"Uspješno obrisana aktivnost!\"}
DELETE,/aktivnost/DM,null,{\"message\":\"Uspješno obrisana aktivnost!\"}
DELETE,/predmet/RMA,null,{\"message\":\"Uspješno obrisan predmet!\"}
DELETE,/predmet/RMA,null,{\"message\":\"Greška - predmet nije obrisan!\"}
DELETE,/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}