var ajax = new XMLHttpRequest();
var ajax2 = new XMLHttpRequest();
var dugme = document.getElementById('dugme');
var poruka = document.getElementById('poruka');

var predmeti = [];
var aktivnosti = [];
//ucitavanje predmeta i aktivnosti
ajax.open('GET','http://localhost:3000/predmeti',true);
ajax2.open('GET','http://localhost:3000/aktivnosti',true);
ajax.onload = function () {
    predmeti = JSON.parse(ajax.responseText);
    console.log(predmeti);
};
ajax2.onload = function () {
    aktivnosti = JSON.parse(ajax2.responseText);
    console.log(aktivnosti);
}; 
ajax.send();
ajax2.send();

dugme.onclick = function() {
    let request= new XMLHttpRequest();
    let request1 = new XMLHttpRequest();
    let predmetInput = document.getElementById('predmet').value;
    let tipInput = document.getElementById('tip').value;
    let pocetakInput = document.getElementById('vrijeme1').value;
    let krajInput = document.getElementById('vrijeme2').value;
    let danInput = document.getElementById('dan').value;
    let objekat = {
        naziv: predmetInput,
        tip: tipInput,
        pocetak: pocetakInput,
        kraj: krajInput,
        dan: danInput
    }
    //provjera da li predmet postoji
    let postojiPredmet = false;
    for (let i = 0; i < predmeti.length; i++) {
        if (predmeti[i].naziv == objekat[predmet]) {
            postojiPredmet = true;
            break;
        }
    }
    //ako predmet ne postoji kreirati ga i onda dodati aktivnost
    if (!postojiPredmet) {
        request1.onreadystatechange = function() {
            if (request1.readyState == 4) {
                request.onreadystatechange = function() {
                    if(request.readyState == 4 && request.status == 200) {
                        poruka.innerHTML = 'Uspješno dodana aktivnost!'
                    } else {
                        let brisi = new XMLHttpRequest()
                        brisi.open('DELETE','http://localhost:3000/predmet/'+predmetInput,true);
                    }
                }
                poruka.innerHTML = 'Uspješno dodana aktivnost!'
                request.open('POST','http://localhost:3000/aktivnost',true);
                request.setRequestHeader('Content-Type','application/json');
                request.send(JSON.stringify(objekat));
            }
        }
        var pr ={
            naziv: predmetInput
        }
        request1.open('POST','http://localhost:3000/predmet',true);
        request1.setRequestHeader('Content-Type','application/json');
        request1.send(JSON.stringify(pr));  
    } else {
        request.onreadystatechange = function() {
            if(request.readyState == 4) {
                console.log('Uspješno dodana aktivnost!');
            } else {
                let brisi = new XMLHttpRequest()
                brisi.open('DELETE','http://localhost:3000/predmet/'+predmetInput,true);
            }
        }
        request.open('POST','http://localhost:3000/aktivnost',true);
        request.setRequestHeader('Content-Type','application/json');
        request.send(JSON.stringify(objekat));
    }
}