function iscrtajRaspored(div,dani,satPocetak,satKraj) {
    if (satPocetak >= satKraj || !Number.isInteger(satKraj) || !Number.isInteger(satPocetak) || satPocetak < 0 || satKraj < 0 || satKraj > 24 || satPocetak > 24 || dani.length == 0) {
        div.innerHTML = "Greška";
        return;
    }

    var tabela = document.createElement("table");
    var red = document.createElement("tr");
    red.className = "prvi";
    var sat;
    //kreiranje prvog reda tabele
    for (var i = 0; i < 3; i++) {
        sat = document.createElement("td");
        red.appendChild(sat);
    }

    for ( i = 0; i < 2 * (satKraj-satPocetak)  ; i++) {
        sat = document.createElement("td");
        if ( (parseInt(satPocetak + i/2) >= 0 && parseInt(satPocetak + i/2) <=12 && parseInt(satPocetak + i/2)%2==0) || (parseInt(satPocetak + i/2) >= 15 && parseInt(satPocetak + i/2) <=23 && parseInt(satPocetak + i/2)%2==1)) {
            sat.colSpan="2";
            var s =  parseInt(satPocetak + i/2);
            if (parseInt(satPocetak + i/2) < 10) {
                s = "0" + s;
            }
            s = s + ":00";
            sat.appendChild(document.createTextNode(s));
            i++;
        }
        red.appendChild(sat);
    }
    tabela.appendChild(red);
    //kreiranje ostalih redova tabele
    for ( i = 0; i < dani.length ; i++) {
      red = document.createElement("tr");
      var dan = document.createElement("td");
      dan.className ="lijevo";
      dan.colSpan="4";
      var ime = document.createTextNode(dani[i]);
      dan.appendChild(ime);
      red.appendChild(dan);
  
      for (var j = 0; j < 2 * (satKraj-satPocetak); j++) {
        var celija = document.createElement("td");
        if (j%2 == 0) celija.className = "pola";
        red.appendChild(celija);
      }

      tabela.appendChild(red);

    }
    
    div.appendChild(tabela);
}

function dodajAktivnost(raspored, naziv, tip, vrijemePocetak, vrijemeKraj,dan) {
    if (raspored == null || raspored.childElementCount == 0) {
        alert("Greška - raspored nije kreiran");
        return;
    }
    if( (!Number.isInteger(vrijemePocetak) && !Number.isInteger(vrijemePocetak-0.5)) || (!Number.isInteger(vrijemeKraj) && !Number.isInteger(vrijemeKraj-0.5) ) ) {
        alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");   
        return;
    }
    var tabela = raspored.firstChild;
    var red = null;
    //trazenje odgovarajuceg reda
    for (var i = 1; i < tabela.childElementCount; i++) {
        if (tabela.childNodes[i].firstChild.innerHTML == dan) {
            red = i;
            break;
        }
    }
    if (red == null || vrijemePocetak >= vrijemeKraj ) {
        alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");  
        return;
    }

    //trazenje sata
    var celijeZaPreskociti;
    var vr;
   
    if (tabela.firstChild.childNodes[3].innerHTML!="") {
        celijeZaPreskociti = (vrijemePocetak - parseInt(tabela.firstChild.childNodes[3].innerHTML))*2;
        vr = (vrijemePocetak - parseInt(tabela.firstChild.childNodes[3].innerHTML))*2 + 1;
        if(vrijemePocetak < parseInt(tabela.firstChild.childNodes[3].innerHTML) ) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");  
            return;
        }
    } else {
        celijeZaPreskociti = (vrijemePocetak - parseInt(tabela.firstChild.childNodes[5].innerHTML))*2 + 2;
        vr = celijeZaPreskociti + 1 ;
        if(vrijemePocetak < parseInt(tabela.firstChild.childNodes[5].innerHTML) -1 ) {
            alert("Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin");  
            return;
        }
    }
     
    var niz = new Array();
    let dodala = false;
    for ( i = 1 ; i < vr + 1 ; i++ ) {
        dodala = false;
        if (tabela.childNodes[red].childNodes[i].innerHTML != "") {
            celijeZaPreskociti = celijeZaPreskociti + 1 - tabela.childNodes[red].childNodes[i].colSpan;
            for ( let k = 0; k < tabela.childNodes[red].childNodes[i].colSpan; k++) {
                niz.push(1);
                dodala = true;
            }
        }
        if (!dodala) {
            niz.push(0);
        }
    }
    if(niz[vr-1] == 1) {
        alert("Greška - već postoji termin u rasporedu u zadanom vremenu");
        return;
    }

    var predmet = document.createElement("h4");
    var aktivnost = document.createElement("h5");
   
    predmet.appendChild(document.createTextNode(naziv));
    aktivnost.appendChild(document.createTextNode(tip));
    
    var brojCelija=(vrijemeKraj-vrijemePocetak)*2;
  
    tabela.childNodes[red].childNodes[celijeZaPreskociti+1].colSpan=brojCelija;
    if (Number.isInteger(vrijemePocetak) && Number.isInteger(vrijemeKraj)) tabela.childNodes[red].childNodes[celijeZaPreskociti+1].className = "";
    if (!Number.isInteger(vrijemeKraj)) tabela.childNodes[red].childNodes[celijeZaPreskociti+1].className = "pola";

    //brisanje viska celija
    for (i=0; i < brojCelija - 1; i++) {
        tabela.childNodes[red].childNodes[celijeZaPreskociti+2].remove();
    }

    tabela.childNodes[red].childNodes[celijeZaPreskociti+1].appendChild(predmet);
    tabela.childNodes[red].childNodes[celijeZaPreskociti+1].appendChild(aktivnost);
}
