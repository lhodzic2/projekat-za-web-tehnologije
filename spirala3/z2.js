const express = require('express');
const fs = require('fs');
const bodyParser = require ( 'body-parser' );
const path = require('path');
const app = express();

app.use(express.static('public'))

app.get('/predmeti',function (req,res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.writeHead(200,{'Content-Type':'application/json'});
        let podaci='';
        let predmeti=''
        fs.readFile('predmeti.txt', 'utf8', function(err, data){ 
            if (err) {
                console.log(err); 
            }
            podaci = data.toString('utf-8');
            predmeti = podaci.split('\n');
            let json = [];
            if (podaci!='' && podaci!='\r' && podaci!='\n')
           { for (let i = 0; i < predmeti.length; i++) {
                let objekat = {};
                objekat['naziv']=predmeti[i].replace('\r','');
                json.push(objekat);
            }}
        
            res.end(JSON.stringify(json));
        });
});

app.get('/aktivnosti',function (req,res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.writeHead(200,{'Content-Type':'application/json'});
    let podaci='';
    let redovi='';
    fs.readFile('aktivnosti.txt', 'utf8', function(err, data){ 
        if (err) {
            console.log(err); 
        }
        podaci = data.toString('utf-8');
        redovi = podaci.split('\n');
        let json = [];
        if (podaci != '' && podaci!='\r' && podaci!='\n') {
        for (let i = 0; i < redovi.length; i++) {
            let niz = redovi[i].split(',');
            let objekat = {};
            objekat['naziv']=niz[0];
            objekat['tip']=niz[1];
            objekat['pocetak']=Number.parseInt(niz[2]);
            objekat['kraj']=Number.parseInt(niz[3]);
            objekat['dan']=niz[4].replace('\r','');
        
            json.push(objekat);
        }
    }
        res.end(JSON.stringify(json));
    });
});

app.get('/predmet/:naziv/aktivnost/',function(req,res) {
    res.writeHead(200,{'Content-Type':'application/json'});
    let podaci='';
    let redovi='';
    let kolone='';

    fs.readFile('aktivnosti.txt', 'utf8', function(err, data){ 
        if (err) {
            console.log(err); 
        }
        podaci = data.toString('utf-8');
        redovi = podaci.split('\n');
        kolone = redovi[0].split(',');
        let json = [];
        
        for (let i = 0; i < redovi.length; i++) {
            let niz = redovi[i].split(',');
            let objekat = {};
            if (niz[0]==req.params.naziv) {
            objekat['tip']=niz[1];
            objekat['pocetak']=Number.parseInt(niz[2]);
            objekat['kraj']=Number.parseInt(niz[3]);
            objekat['dan']=niz[4].replace('\r','');
            json.push(objekat);
            }
        }
        res.end(JSON.stringify(json));
    });
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.post( '/predmet' , function( req , res ){
    let tijelo = req.body;
    let m = fs.readFileSync('predmeti.txt','utf-8')
    let novaLinija = tijelo["naziv"];
    if (m == '' || m =='\n' || m=='\r') novaLinija = novaLinija;
    else novaLinija = "\n" + novaLinija;
    var podaci = '';
    var redovi = '';
    var p = false;
    fs.readFile('predmeti.txt', function(err, data){ 
        if (err) {
            console.log(err); 
        }
        podaci = data.toString('utf-8');
        redovi = podaci.split('\n');

        for (let i = 0; i < redovi.length; i++) {
            if (redovi[i] == tijelo["naziv"]) {
                p = true;  
            }
        }
    if(!p) {
        fs.appendFile ('predmeti.txt' , novaLinija , function ( err ){
        if (err) {
            throw err ;
        }
        res.json({message:"Uspješno dodan predmet!"});
        });
    } else {
        res.json({message:"Naziv predmeta postoji!"}); 
    }

    });
  
    
});

app.post('/aktivnost', function( req , res ){
    let tijelo = req.body;
    let podaci = fs.readFileSync('aktivnosti.txt','utf-8')
    let novaLinija = tijelo["naziv"] + "," + tijelo["tip"] + "," + tijelo["pocetak"] + "," + tijelo["kraj"] + "," + tijelo["dan"];
    if (podaci == '' || podaci =='\n' || podaci=='\r') novaLinija = novaLinija;
    else novaLinija = "\n" + novaLinija;
    let niz = novaLinija.split(',')
    if (niz.length == 5 && typeof niz[0] == 'string' && Number.isInteger(Number.parseInt(niz[2])) && Number.isInteger(Number.parseInt(niz[3])) && typeof niz[4]=='string') {
        fs.appendFile ('aktivnosti.txt' , novaLinija , function ( err ){
            if (err) {
                throw err ;
            }
            res.json({message:"Uspješno dodana aktivnost!"});
            });  
    } else {
            res.json({message: "Aktivnost nije validna!"});
    }
    
});

app.delete('/aktivnost/:naziv',function(req,res){
    fs.readFile('aktivnosti.txt', 'utf8', function(err, data)
    {
        if (err)
        {
            res.json({message:"Greška - aktivnost nije obrisana!"});
        }
        let podaci = data.toString('utf-8');
        let redovi = podaci.split('\n');
        let zapisi = '';
        let nadjeno = false;
        for (let i = 0; i < redovi.length; i++) {
            let niz = redovi[i].split(',');
            if (req.params.naziv != niz[0].replace('\n','')) zapisi = zapisi + redovi[i] + "\n";
            if (req.params.naziv == niz[0].replace('\n','')) nadjeno = true;
        }
        fs.writeFile('aktivnosti.txt', zapisi,'utf-8',function(err) {
            if (err || !nadjeno) res.json({message:"Greška - aktivnost nije obrisana!"});
            else res.json({message:"Uspješno obrisana aktivnost!"}) 
        });
    });
});

app.delete('/predmet/:naziv',function(req,res){
    fs.readFile('predmeti.txt', 'utf8', function(err, data)
    {
        if (err)
        {
            res.json({message:"Greška - predmet nije obrisan!"});
        }
        let podaci = data.toString('utf-8');
        let redovi = podaci.split('\n');
        let zapisi = ''
        let nadjeno = false;
        for (let i = 0; i < redovi.length; i++) {
            if (req.params.naziv != redovi[i].replace('\n','').replace('\r','')) zapisi = zapisi + redovi[i] + "\n";
            if (req.params.naziv == redovi[i].replace('\n','').replace('\r','')) nadjeno = true;
        }
        fs.writeFile('predmeti.txt', zapisi,'utf-8',function(err) {
            if (err || !nadjeno) res.json({message:"Greška - predmet nije obrisan!"});
            else res.json({message:"Uspješno obrisan predmet!"}) 
        });
    });
});


app.delete('/all',function(req,res) {
    let greska = false;
    fs.readFile('predmeti.txt', 'utf8', function(err, data)
    {
        var podaci = data.toString('utf-8');
        if (err || podaci == '')
        {
            greska = true;
        }
        fs.writeFile('predmeti.txt', '','utf-8',  function(err) {
            if (err) greska = true;
        });  
    });
    fs.readFile('aktivnosti.txt', 'utf8', function(err, data)
    {
        var podaci = data.toString('utf-8');
        if (err || podaci == '')
        {
            greska = true;
        }
        fs.writeFile('aktivnosti.txt', '','utf-8', function(err) {
            if (err) greska = true;
        });
    });
    if (!greska) res.json({message:"Uspješno obrisan sadržaj datoteka!"});
    else res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"});
});

app.get('/unosRasporeda.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/unosRasporeda.html"));
});

var server = app.listen(3000);
module.exports = server;