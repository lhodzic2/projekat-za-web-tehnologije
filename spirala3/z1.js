const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();

app.use(express.static('public'))

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/spirala2rasporedi.html"));
});

app.get('/spirala2rasporedi.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/spirala2rasporedi.html"));
});

app.get('/aktivnost.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/aktivnost.html"));
});

app.get('/planiranjeNastavnik.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/planiranjeNastavnik.html"));
});

app.get('/podaciStudent.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/podaciStudent.html"));
});
app.get('/unosRasporeda.html', function(req, res) {
    res.sendFile(path.join(__dirname + "/public/unosRasporeda.html"));
});

app.listen(3000);