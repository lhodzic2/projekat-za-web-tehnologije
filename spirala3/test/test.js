let chai = require('chai');
let fs = require('fs');
let chaiHttp = require('chai-http');
let server = require('../z2.js');
const { response } = require('express');
module.exports = server;
let assert = chai.assert;

chai.should();
chai.use(chaiHttp);

let podaci = fs.readFileSync('testniPodaci.txt','utf-8');
let redovi = podaci.toString('utf-8').split('\n');

for (let i=0; i < redovi.length; i++) {
    let pom = redovi[i].split(',');
    let metoda = pom[0];
    let ruta = pom[1];
    let ulaz=pom[2];
    let izlaz='';
    let string='';
    if (pom.length > 4 && ulaz != 'null') {
        for (let j = 2; j < pom.length; j++)
        {
            string = string + pom[j];
            if (j != pom.length - 1) string = string + ',';
        }
        let substring = string.split("},");
        ulaz = substring[0]+"}"
        izlaz = substring[1];
    } else if (pom.length > 4 && ulaz == 'null') {
        izlaz = pom[3];   
        izlaz = izlaz +",";
        for (let j = 4; j < pom.length; j++){
            izlaz = izlaz + pom[j];
            if (j != pom.length-1) izlaz = izlaz + "," ;
        }         
    } else {
        ulaz = pom[2];
        izlaz = pom[3];
    }
    if (ulaz != null) ulaz=ulaz.replace(/\\/g,'');
    izlaz = izlaz.replace(/\\/gm,'').replace('\r','');

    describe("z2.js", () => {
        describe('metode ', function () {
            if (metoda == 'GET') {
                it(metoda,(done) => {   
                    chai.request(server)
                        .get(ruta)
                        .end(  (err, response)=> {
                            response.should.have.status(200);
                            assert.equal(izlaz,JSON.stringify(response.body));
                            done();
                        })
                });
            } else if (metoda == 'DELETE') {
                it(metoda,(done) => { 
                    chai.request(server)
                        .delete(ruta)
                        .end((err,response) => {
                            assert.equal(izlaz,JSON.stringify(response.body));
                            done();
                        })
                });
            } else if (metoda == 'POST') {
                it(metoda,(done) => { 
                    chai.request(server)
                        .post(ruta)
                        .send(JSON.parse(ulaz))
                        .end((err,response) => {
                            assert.equal(izlaz,JSON.stringify(response.body));
                            done();
                        })
                });
            }
        });

    });
}